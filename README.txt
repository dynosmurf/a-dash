



################################
 DATA STRUCTURE
################################

--------------------------------
 Requests
--------------------------------

collection of requests

{
    url : http://www.tucsonweekly.com/tucson/LocationSection?oid=909090,
    params : [oid],
    pairs : [oid=909090]
    timestamp : 149112931
}


--------------------------------
  Index
--------------------------------

unique collection of attributes

i.e.

id, key       , value
01, url       , www.google.com
01, param     , locationSection
-0, timestamp , 149982394

Allows conversion between short id and actual value.

Short ids are base 64 encoded incremented values.


--------------------------------
  Transform
--------------------------------

convenience collection of transformed requests.

{
    url : http://www.tucsonweekly.com/tucson/LocationSection?oid=909090,
    params : [oid],
    pairs : [oid=909090]
    timestamp : 149112931
}

becomes

{
    url : T
    params : [01],
    paris : [0-],
    timestamp : 149112931
}


--------------------------------
  DataAssets
--------------------------------

Collection to manage flat file representation of data for use on the client.

csv formated output of transform collection



################################
 PACKAGES
################################

--------------------------------
  data-manager
--------------------------------

Package to insert data in the various data structures.
