build_ui = function(vectors) {

    // Various formatters.
    var formatNumber = d3.format(",d"),
        formatChange = d3.format("+,d"),
        formatDate = d3.time.format("%B %d, %Y"),
        formatTime = d3.time.format("%I:%M %p");

    // A nest operator, for grouping the request list.
    var nestByRequest = d3.nest()
        .key(function(v) {
            return v.r_id;
        });


    // Create the crossfilter for the relevant dimensions and groups.
    context = crossfilter(vectors);
    // dimensions
    hour = context.dimension(function(v) {
        var h = new Date(v.time);
        return h.getHours() + h.getMinutes() / 60;
    });

    url = context.dimension(function(v) {
        return v.url;
    });

    param = context.dimension(function(v) {
        return v.param;
    });

    pair = context.dimension(function(v) {
        return v.param + '=' + v.value;
    });

    request = context.dimension(function(v) {
        return v.url + ',' + v.r_id;
    });

    // groups
    all = context.groupAll();
    urls = url.group(function(url) {
        return url;
    });
    hours = hour.group(Math.floor);
    params = param.group(function(p) {
        return p;
    });
    requests = request.group(function(r) {
        return r;
    });


    var charts = [
        // request chart
        barChart()
        .dimension(hour)
        .group(hours)
        .x(d3.scale.linear()
            .domain([0, 24])
            .rangeRound([0, 10 * 24])
        ),

        // parameter chart
        barChart()
        .dimension(param)
        .group(params)
        .x(d3.scale.ordinal()
            .domain(_.pluck(params.top(40), 'key'))
            .rangeBands([0, 10 * 40])
        ),
        /*
        barChart()
        .dimension(distance)
        .group(distances)
        .x(d3.scale.linear()
            .domain([0, 2000])
            .rangeRound([0, 10 * 40])),

        barChart()
        .dimension(date)
        .group(dates)
        .round(d3.time.day.round)
        .x(d3.time.scale()
            .domain([new Date(2001, 0, 1), new Date(2001, 3, 1)])
            .rangeRound([0, 10 * 90]))
        .filter([new Date(2001, 1, 1), new Date(2001, 2, 1)])
        */
    ];

    // Given our array of charts, which we assume are in the same order as the
    // .chart elements in the DOM, bind the charts to the DOM and render them.
    // We also listen to the chart's brush events to update the display.
    /*
    var chart = d3.selectAll(".chart")
        .data(charts)
        .each(function(chart) {
            chart.on("brush", renderAll).on("brushend", renderAll);
        });
        */
    time_chart = d3.selectAll(".time-chart")
        .data(
            barChart()
            .dimension(hour)
            .group(hours)
            .x(d3.scale.linear()
                .domain([0, 24])
                .rangeRound([0, 10 * 24])
            )
        ).on("brush", renderAll).on("brushend", renderAll);

    param_chart = d3.selectAll(".param-chart")
        .data(
            barChart()
            .dimension(param)
            .group(params)
            .x(d3.scale.ordinal()
                .domain(_.pluck(params.top(40), 'key'))
                .rangeBands([0, 10 * 40])
            )
        ).on("brush", renderAll).on("brushend", renderAll);

    // Render the initial lists.
    var list = d3.selectAll(".list")
        .data([requestList]);

    // Render the total.
    d3.selectAll("#total")
        .text(formatNumber(requests.size()));

    renderAll();

    // Renders the specified chart or list.
    function render(method) {
        d3.select(this).call(method);
    }

    // Whenever the brush moves, re-rendering everything.
    function renderAll() {

        time_chart.render();
        param_chart.render();
        list.each(render);
        d3.select("#active").text(formatNumber(all.value()));
    }

    // Like d3.time.format, but faster.
    function parseDate(d) {
        return new Date(2001,
            d.substring(0, 2) - 1,
            d.substring(2, 4),
            d.substring(4, 6),
            d.substring(6, 8));
    }

    window.filter = function(filters) {
        filters.forEach(function(d, i) {
            charts[i].filter(d);
        });
        renderAll();
    };

    window.reset = function(i) {
        charts[i].filter(null);
        renderAll();
    };

    function requestList(div) {
        var requestsByHour = nestByRequest.entries(hour.top(40));

        div.each(function() {
            var date = d3.select(this).selectAll(".date")
                .data(requestsByHour, function(d) {
                    return d.key;
                });

            date.enter().append("div")
                .attr("class", "date")
                .append("div")
                .attr("class", "day")
                .text(function(d) {

                    return formatDate(new Date(d.values[0].time));
                });

            date.exit().remove();

            var request = date.order().selectAll(".request")
                .data(function(d) {
                    return d.values;
                }, function(d) {
                    return d.index;
                });

            var requestEnter = request.enter().append("div")
                .attr("class", "request");

            requestEnter.append("div")
                .attr("class", "time")
                .text(function(d) {
                    return formatTime(new Date(d.time));
                });

            requestEnter.append("div")
                .attr("class", "url")
                .text(function(d) {
                    return d.url;
                });



            /*
                        requestEnter.append("div")
                            .attr("class", "params")
                            .classed("early", function(d) {
                                return d.delay < 0;
                            })
                            .text(function(d) {
                                return formatChange(d.delay) + " min.";
                            });
            */
            request.exit().remove();

            request.order();
        });
    }

    function barChart() {
        if (!barChart.id) barChart.id = 0;

        var margin = {
                top: 40,
                right: 40,
                bottom: 100,
                left: 40
            },
            x,
            y = d3.scale.linear().range([100, 0]),
            id = barChart.id++,
            axis = d3.svg.axis().orient("bottom"),
            brush = d3.svg.brush(),
            brushDirty,
            dimension,
            group,
            round;

        function chart(div) {
            var width = x.range()[x.range().length - 1],
                height = y.range()[0];
            //console.log('>>>', group.top(1), group.top(1)[0]);
            y.domain([0, group.top(1)[0].value]);

            div.each(function() {
                var div = d3.select(this),
                    g = div.select("g");

                // Create the skeletal chart.
                if (g.empty()) {
                    div.select(".title").append("a")
                        .attr("href", "javascript:reset(" + id + ")")
                        .attr("class", "reset")
                        .text("reset")
                        .style("display", "none");

                    g = div.append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    g.append("clipPath")
                        .attr("id", "clip-" + id)
                        .append("rect")
                        .attr("width", width)
                        .attr("height", height);

                    g.selectAll(".bar")
                        .data(["background", "foreground"])
                        .enter().append("path")
                        .attr("class", function(d) {
                            return d + " bar";
                        })
                        .datum(group.all());

                    g.selectAll(".foreground.bar")
                        .attr("clip-path", "url(#clip-" + id + ")");

                    g.append("g")
                        .attr("class", "axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(axis)
                        .selectAll("text")
                        .style("text-anchor", "end")
                        .attr("dx", "-.8em")
                        .attr("dy", ".15em")
                        .attr("transform", function(d) {
                            return "rotate(-65)"
                        });

                    // Initialize the brush component with pretty resize handles.
                    var gBrush = g.append("g").attr("class", "brush").call(brush);
                    gBrush.selectAll("rect").attr("height", height);
                    gBrush.selectAll(".resize").append("path").attr("d", resizePath);
                }

                // Only redraw the brush if set externally.
                if (brushDirty) {
                    brushDirty = false;
                    g.selectAll(".brush").call(brush);
                    div.select(".title a").style("display", brush.empty() ? "none" : null);
                    if (brush.empty()) {
                        g.selectAll("#clip-" + id + " rect")
                            .attr("x", 0)
                            .attr("width", width);
                    } else {
                        var extent = brush.extent();
                        g.selectAll("#clip-" + id + " rect")
                            .attr("x", x(extent[0]))
                            .attr("width", x(extent[1]) - x(extent[0]));
                    }
                }

                g.selectAll(".bar").attr("d", barPath);
            });

            function barPath(groups) {
                var path = [],
                    i = -1,
                    n = groups.length,
                    d;
                while (++i < n) {
                    d = groups[i];
                    path.push("M", x(d.key), ",", height, "V", y(d.value), "h9V", height);
                }
                return path.join("");
            }

            function resizePath(d) {
                var e = +(d == "e"),
                    x = e ? 1 : -1,
                    y = height / 3;
                return "M" + (.5 * x) + "," + y + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6) + "V" + (2 * y - 6) + "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y) + "Z" + "M" + (2.5 * x) + "," + (y + 8) + "V" + (2 * y - 8) + "M" + (4.5 * x) + "," + (y + 8) + "V" + (2 * y - 8);
            }
        }

        brush.on("brushstart.chart", function() {
            var div = d3.select(this.parentNode.parentNode.parentNode);
            div.select(".title a").style("display", null);
        });

        brush.on("brush.chart", function() {

            var g = d3.select(this.parentNode),
                extent = brush.extent();
            if (round) {
                g.select(".brush")
                    .call(brush.extent(extent = extent.map(round)))
                    .selectAll(".resize")
                    .style("display", null);
            }
            if (typeof x(0) == "undefined") {
                g.select("#clip-" + id + " rect")
                    .attr("x", extent[0])
                    .attr("width", extent[1] - extent[0]);
                var range = x.range();
                var domain = x.domain();
                var sub_domains = [];
                _.each(range, function(a, i) {
                    if (a >= extent[0] && a <= extent[1]) {
                        sub_domains.push(domain[i]);
                    }
                });
                console.log(sub_domains);


                dimension.filter(function(d) {
                    if (_.indexOf(sub_domains, d) < 0) {
                        return false;
                    }
                    return true;
                });

            } else {
                g.select("#clip-" + id + " rect")
                    .attr("x", x(extent[0]))
                    .attr("width", x(extent[1]) - x(extent[0]));
                dimension.filterRange(extent);
            }

            test_d = dimension;
            test_x = x;

        });

        brush.on("brushend.chart", function() {
            if (brush.empty()) {
                var div = d3.select(this.parentNode.parentNode.parentNode);
                div.select(".title a").style("display", "none");
                div.select("#clip-" + id + " rect").attr("x", null).attr("width", "100%");
                //dimension.filterAll();
            }
        });

        chart.margin = function(_) {
            if (!arguments.length) return margin;
            margin = _;
            return chart;
        };

        chart.x = function(_) {
            if (!arguments.length) return x;
            x = _;
            axis.scale(x);
            brush.x(x);
            return chart;
        };

        chart.y = function(_) {
            if (!arguments.length) return y;
            y = _;
            return chart;
        };

        chart.dimension = function(_) {
            if (!arguments.length) return dimension;
            dimension = _;
            return chart;
        };

        chart.filter = function(_) {
            if (_) {
                brush.extent(_);
                dimension.filterRange(_);
            } else {
                brush.clear();
                dimension.filterAll();
            }
            brushDirty = true;
            return chart;
        };

        chart.group = function(_) {
            if (!arguments.length) return group;
            group = _;
            return chart;
        };

        chart.round = function(_) {
            if (!arguments.length) return round;
            round = _;
            return chart;
        };

        return d3.rebind(chart, brush, "on");
    }




};


sort_chart = function() {

    var modifier = {
        fields: {
            value: 1,
            site_id: 1
        }
    };
    var sortModifier = Session.get('barChartSortModifier');
    if (sortModifier && sortModifier.sort)
        modifier.sort = sortModifier.sort;

    var dataset = Bars.find({}, modifier).fetch();

    //Update scale domains
    var domain = dataset.map(function(d) {
        var site = Sites.findOne({
            _id: d.site_id
        });
        if (site) {
            return site.name;
        } else {
            return "--";
        }

    });

    xScale.domain(domain);
    yScale.domain([0, d3.max(dataset, function(d) {
        return d.value;
    })]);

    //Select…
    var bars = svg.selectAll("rect")
        .data(dataset, key);

    bars.enter()
        .append("rect")
        .attr("x", w)
        .attr("y", function(d) {
            return h - yScale(d.value);
        })
        .attr("width", xScale.rangeBand())
        .attr("height", function(d) {
            return yScale(d.value);
        })
        .attr("fill", function(d) {
            return "rgb(0, 0, " + (d.value * 10) + ")";
        })
        .attr("data-id", function(d) {
            return d._id;
        });


    //Update…
    bars.transition()
        // .delay(function(d, i) {
        //  return i / dataset.length * 1000;
        // }) // this delay will make transistions sequential instead of paralle
        .duration(500)
        .attr("x", function(d, i) {
            return xScale(domain[i]); //xScale(i);
        })
        .attr("y", function(d) {
            return h - yScale(d.value);
        })
        .attr("width", xScale.rangeBand())
        .attr("height", function(d) {
            return yScale(d.value);
        }).attr("fill", function(d) {
            return "rgb(0, 0, " + (d.value * 10) + ")";
        });

    //Exit…
    bars.exit()
        .transition()
        .duration(500)
        .attr("x", -xScale.rangeBand())
        .remove();



    //Update all labels

    //Select…
    var labels = svg.selectAll("text")
        .data(dataset, key);

    //Enter…
    labels.enter()
        .append("text")
        .text(function(d) {
            return d.value;
        })
        .attr("text-anchor", "middle")
        .attr("x", w)
        .attr("y", function(d) {
            return h - yScale(d.value) + 14;
        })
        .attr("font-family", "sans-serif")
        .attr("font-size", "11px")
        .attr("fill", "white");

    //Update…

    labels.transition()
        // .delay(function(d, i) {
        //  return i / dataset.length * 1000;
        // }) // this delay will make transistions sequential instead of paralle
        .duration(500)
        .attr("x", function(d, i) {
            return xScale(domain[i]) + xScale.rangeBand() / 2;
        }).attr("y", function(d) {
            return h - yScale(d.value) + 14;
        }).text(function(d) {
            return d.value;
        });

    //Exit…
    labels.exit()
        .transition()
        .duration(500)
        .attr("x", -xScale.rangeBand())
        .remove();

    var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient("bottom");

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + h + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", function(d) {
            return "rotate(-65)"
        });

}
