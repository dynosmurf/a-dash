var connect = Npm.require('connect');
var fs = Npm.require('fs');
var path = Npm.require('path');


/**
 * FileCollection
 *
 * Wrapper around Mongo.Collection that allows files to be managed and server
 * to an end user.
 */


FileCollection = function(name, options) {
    // check that data folder is availible

    // create collection to store metadata
    this.name = name;
    this.options = options || {};
    this.path = '.'+(this.options.path || 'data');
    this.collection = new Meteor.Collection(name);
    this.directory = options.directory || '/d/';

    this._setup();

};

FileCollection.prototype._create_path = function(file_path){
    var app_path = path.resolve('.').split('.meteor')[0];
    this.full_path = app_path +file_path + "/";

    try {
        fs.mkdirSync(this.full_path);
    } catch (e) {
        //console.log(e);
    }
};

FileCollection.prototype._setup = function() {
    var directory = this.directory;

    // make sure directory exists.
    this._create_path(this.path);

    //console.log("FileCollection: SERVING " + this.name + " at " + directory);

    RoutePolicy.declare(directory, 'network');

    // Listen to incoming http requests
    WebApp.connectHandlers
        .use(connect.bodyParser())
        .use(directory, connect.static(this.full_path));
};

FileCollection.prototype.insert = function(file_data, meta_data) {
    meta_data = meta_data || {};
    var file_name;
    var id = this.collection.insert(meta_data);
    var filetype = this.options.filetype || "txt";

    // add file.
    if (typeof this.options.fnamegen == 'function') {
        filename = 'tesadgasdf';
    } else {
        filename = id.concat(".", filetype);
    }

    var encoding = meta_data.encoding || this.options.encoding || 'utf8';
    var callback = this.options.on_insert || null;

    // xxx make async
    fs.writeFileSync(this.full_path + "/" + filename, file_data, encoding);

    //console.log('FileCollection: successfully created ' + this.path + '' + filename);

    this.collection.update({
        _id: id
    }, {
        $set: {
            filename: filename
            
        }
    });

    return id;
};

FileCollection.prototype.remove = function(id) {
    var filename = this.collection.findOne(id).filename;

    fs.unlinkSync(this.full_path + filename);

    //console.log('FileCollection: successfully deleted ' + this.path + '/' + filename);

    this.collection.remove(id);

    return id;
};

FileCollection.prototype.removeSet = function(sel) {
    var files = this.collection.find(sel).fetch();

    var ids = _.map(files, function(file) {
        return this.remove(file._id);
    }.bind(this));

    return ids;
};

FileCollection.prototype.update = function(id, data) {
    var file = this.collection.findOne(id);

    var filename = file.filename;
    var encoding = file.encoding;
    var callback = this.options.update_callback || null;

    fs.writeFileSync(this.path + "/" + filename, data, encoding, callback);


    //console.log('FileCollection: successfully UPDATED ' + this.path + '' + filename);

    return id;
};

FileCollection.prototype.get_path = function(id) {
    var filename = this.collection.findOne(id).filename;
    return this.full_path + filename;
};

FileCollection.prototype.get_url = function(id) {
    var filename = this.collection.findOne(id).filename;
    return this.directory + filename;

};
