Package.describe({
    name: 'murphy:file-collection',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Npm.depends({
    //"selenium-standalone": "4.5.3",
    //"webdriverio": "3.1.0",
    //"node-png": "0.4.3",
    //"async": "1.4.2",
    'connect': '2.11.0',
    'request':'2.60.0',

});

Package.onUse(function(api) {
    api.versionsFrom('1.1.0.3');
    api.use(['routepolicy', 'webapp'], 'server');
    api.addFiles('FileCollection.js', 'server');
    api.export(["FileCollection"], 'server');
});

Package.onTest(function(api) {
    api.use('tinytest');
    api.use(['routepolicy', 'webapp'], 'server');
    //api.use('murphy:selenium-wrapper');
    api.addFiles([
        'FileCollection.js',
        'test_file_collection.js'
    ], 'server');
});
