var fs = Npm.require('fs');
var request = Npm.require('request');

Tinytest.add('FileCollection - init', function(test) {
    var test_file_collection = new FileCollection('test_name1', {
        path: 'test_path',
        directory: '/test_directory/'
    });
    test.equal(test_file_collection.name, 'test_name1');
});

Tinytest.add('FileCollection - insert', function(test) {
    var test_file_collection = new FileCollection('test_name2', {
        path: 'test_path',
        directory: '/test_directory/',
        filetype: 'txt'
    });
    var prior_count = test_file_collection.collection.find().count();
    var file_content = 'test test test test';
    var id = test_file_collection.insert(file_content, {
        'meta1': 'test',
        'meta2': 'test'
    });

    var post_count = test_file_collection.collection.find().count();

    test.equal(prior_count + 1, post_count);

    var file_item = test_file_collection.collection.findOne(id);
    var file = fs.readFileSync(test_file_collection.full_path + "/" + file_item.filename, "utf8");

    test.equal(file_content, file);
});


Tinytest.add('FileCollection - remove', function(test) {
    var test_file_collection = new FileCollection('test_name3', {
        path: 'test_path',
        directory: '/test_directory/',
        filetype: 'txt'
    });
    var prior_count = test_file_collection.collection.find().count();
    var file_content = 'test test test test';
    var id = test_file_collection.insert(file_content, {
        'meta1': 'test',
        'meta2': 'test'
    });

    var post_count = test_file_collection.collection.find().count();

    test.equal(prior_count + 1, post_count);
    // get file infor to check if file is removed
    var file_item = test_file_collection.collection.findOne(id);
    var file_path = test_file_collection.full_path + file_item.filename;

    test_file_collection.remove(id);

    var poster_count = test_file_collection.collection.find().count();

    test.equal(prior_count, poster_count);
    var exists = true;
    try {
        console.log(fs.statSync(file_path));
    } catch (e) {
        var exists = false;
    }
    test.isFalse(exists);
});


Tinytest.add('FileCollection - removeSet xxx', function(test) {
    // xxx
});


Tinytest.add('FileCollection - getPath', function(test) {
    // xxx

    var test_file_collection = new FileCollection('test_name4', {
        path: 'test_path',
        directory: '/test_directory/',
        filetype: 'txt'
    });

    var file_content = 'test test test test';
    var id = test_file_collection.insert(file_content, {
        'meta1': 'test',
        'meta2': 'test'
    });

    var result = test_file_collection.getPath(id);

    test.equal(result, test_file_collection.full_path + id + '.txt');

});

Tinytest.add('FileCollection - getUrl', function(test) {
    // xxx

    var test_file_collection = new FileCollection('test_name5', {
        path: 'test_path',
        directory: '/test_directory/',
        filetype: 'txt'
    });

    var file_content = 'test test test test';
    var id = test_file_collection.insert(file_content, {
        'meta1': 'test',
        'meta2': 'test'
    });

    var result = test_file_collection.getUrl(id);

    test.equal(result, test_file_collection.directory + id + '.txt');

});


Tinytest.addAsync('FileCollection - get file from server', function(test, done) {
    var test_file_collection = new FileCollection('test_name6', {
        path: 'test_path',
        directory: '/p/',
        filetype: 'txt'
    });

    var file_content = 'test test test test';
    var id = test_file_collection.insert(file_content, {
        'meta1': 'test',
        'meta2': 'test'
    });

    var result = test_file_collection.getUrl(id);

    test.equal(result, test_file_collection.directory + id + '.txt');

    request('http://localhost:3000/p/' + id + '.txt', Meteor.bindEnvironment(function(error, response, body) {

        test.equal(body, file_content);
        done();
    }));

});
