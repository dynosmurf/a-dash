Package.describe({
    name: 'murphy:data-manager',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Npm.depends({
    "radix-64": "1.0.4",
});

Package.onUse(function(api) {
    api.versionsFrom('1.1.0.3');
    api.use([
        'routepolicy',
        'matb33:collection-hooks',
        'webapp',
        'underscore'
    ], 'server');
    api.addFiles('index-controller.js', 'server');
    api.export(["IndexController"], 'server');
});

Package.onTest(function(api) {
    api.use('tinytest');
    api.use(['routepolicy','underscore', 'livedata', 'webapp'], 'server');
    api.use('murphy:data-manager');
    api.addFiles([
        'test.js'
    ], 'server');
});
