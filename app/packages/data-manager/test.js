var test_index = new Meteor.Collection('test_index');
var test_transform = new Meteor.Collection('test_transform');
var test_source = new Meteor.Collection('test_source');
var file_collection_stub = {
    find: function() {},
    get_path: function() {},
    get_dir: function() {}
};

var get_inst = function() {
    var test_ct = new IndexController({
        source_collection: test_source,
        index_collection: test_index,
        file_collection: file_collection_stub,
        transform_collection : test_transform,
    });
    return test_ct;
};

Tinytest.add('init', function(test) {
    var test_ct = get_inst();
    test.isNotUndefined(test_ct.source);
    test.isNotUndefined(test_ct.collection);
    test.isNotUndefined(test_ct.file_collection);
});

Tinytest.add('_increment_id', function(test) {
    var test_ct = get_inst();

    var test_id = test_ct._increment_id('-');
    test.equal(test_id, '0');

    test_id = test_ct._increment_id('0');
    test.equal(test_id, '1');

    test_id = test_ct._increment_id('1');
    test.equal(test_id, '2');

    // empty input
    test_id = test_ct._increment_id();
    test.equal(test_id, '-');

    // zero
    //test_id = test_ct._increment_id(0)

    //test.equal(test_id, '1');
});

Tinytest.add('add_record', function(test) {
    test_index.remove({});
    var test_ct = get_inst();
    var test_field = 'name';
    var test_record = 'test';

    // add record to empy index
    test_ct.add_record(test_field, test_record, false);

    test.equal(test_index.find().count(), 1);

    var index_val = test_index.findOne();
    test.equal(index_val.d, 'name');
    test.equal(index_val.r, 'test');
    test.equal(index_val.id, '-');

    // try to add duplicate record.
    var out = test_ct.add_record(test_field, test_record, false);

    test.equal(out, '-');
    test.equal(test_index.find().count(), 1);

    index_val = test_index.findOne();
    test.equal(index_val.d, 'name');
    test.equal(index_val.r, 'test');
    test.equal(index_val.id, '-');

    // add another record
    test_ct.add_record(test_field, 'test2', false);

    test.equal(test_index.find().count(), 2);

    index_val = test_index.find().fetch()[1];
    test.equal(index_val.d, 'name');
    test.equal(index_val.r, 'test2');
    test.equal(index_val.id, '0');

    // add another record
    test_ct.add_record(test_field, 'test3', false);

    test.equal(test_index.find().count(), 3);

    index_val = test_index.find().fetch()[2];
    test.equal(index_val.d, 'name');
    test.equal(index_val.r, 'test3');
    test.equal(index_val.id, '1');

    // test a bunch
    for (var i = 0; i < 100; i++) {
        test_ct.add_record('name', 'test' + i, false);
    }

    // preserve
    test_index.remove({});

    test_ct.add_record('name', 'test', true);

    test.equal(test_index.find().count(), 1);
    test.equal(test_index.findOne().id, 'test');

});

Tinytest.add('index_item', function(test) {
    test_index.remove({});
    test_transform.remove({});
    var test_ct = get_inst();
    var i = 2;
    var fields = [{
        propname: 'url',
        preserve: false,
    }, {
        propname: 'name',
        preserve: true,
    }, {
        propname: 'pirate',
        preserve: false
    }]
    test_ct.set_fields(fields);
    var item = {
        'url': 'link' + i,
        'name': 'test' + i,
        'pirate': ['a', 'b', 'c', '-' + i]
    };
    i = 4
    var item2 = {
        'url': 'link' + i,
        'name': 'test' + i,
        'pirate': ['a', 'b', 'c', '-' + i]
    };
    test_ct.index_item(item);

    test.equal(test_index.find().count(), 6);
    test.equal(test_transform.find().count(), 1);

    test_ct.index_item(item2);

    test.equal(test_index.find().count(), 9);
    test.equal(test_transform.find().count(), 2);
    console.log(test_transform.findOne());
})

Tinytest.add('rebuild_index', function(test) {
    var test_ct = get_inst();

    test_index.remove({});
    test_source.remove({});
    // test a bunch

    for (var i = 0; i < 100; i++) {
        test_source.insert({
            'url': 'link' + i,
            'name': 'test' + i,
            'pirate': ['a', 'b', 'c', '-' + i]
        });
    }

    test_ct.rebuild_index('name', false);

    test.equal(test_index.find().count(), 100);

    var ids = _.uniq(_.pluck(test_index.find().fetch(), 'id'));
    test.equal(ids.length, 100);

    test_ct.rebuild_index('url', true);

    test.equal(test_index.find().count(), 200);
    var ids = _.uniq(_.pluck(test_index.find({
        d: 'url'
    }).fetch(), 'id'));
    test.equal(ids.length, 100);

    test_ct.rebuild_index('pirate', false)

    test.equal(test_index.find().count(), 303);
    var ids = _.uniq(_.pluck(test_index.find({
        d: 'pirate'
    }).fetch(), 'id'));
    test.equal(ids.length, 103);
});

Tinytest.add('get_compressed', function(test) {
    var test_ct = get_inst();
    // use index from previous test
    test.equal(test_index.find().count(), 303);

    var comp = test_ct.get_compressed();

    console.log('COMPRESSD');
    console.log(comp);
    
    test.isNotUndefined(comp[0].url)
    test.isNotUndefined(comp[0].name)
    test.isNotUndefined(comp[0].pirate)

    test.equal(comp[0].pirate.length, 4);
    test.equal(comp.length, test_source.find().count());
})
