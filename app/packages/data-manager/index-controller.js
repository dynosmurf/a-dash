fs = Npm.require('fs');

Math.log2 = Math.log2 || function(x) {
    return Math.log(x) / Math.LN2;
};

var radix64 = Npm.require('radix-64')();

var fast_hash = function(string){
    var hash = 0, i, chr, len;
    if (string.length === 0) return hash;
    for (i = 0, len = string.length; i < len; i++) {
        chr   = string.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

IndexController = function(options) {
    this.source = options.source_collection;
    this.collection = options.index_collection;
    this.sort = options.sort;
    this.file_collection = options.file_collection;
    this.transform_collection = options.transform_collection;

    this.collection.before.insert(function(u, doc){
        doc.t = Date.now();
    });

    this.transform_collection.before.insert(function(u, doc){
        doc.t = Date.now();
    });
    this.fields = options.fields || [];

    this._build_temp_index();

    this.index_to_insert = [];
    this.transform_to_insert = [];
};

IndexController.prototype.set_fields = function(fields){
    this.fields = fields;
};

IndexController.prototype.persist_to_db = function(){
    if(this.index_to_insert.length > 0){
            this.collection.batchInsert(this.index_to_insert);
    }
    if(this.transform_to_insert.length > 0){
        this.transform_collection.batchInsert(this.transform_to_insert);
    }

    this.index_to_insert = [];
    this.transform_to_insert = [];
};

IndexController.prototype.dimension = function(key){
    return _.pluck(this.collection.find({d: key}, {fields : {r: true}}).fetch(), 'r');
};

IndexController.prototype.clear_all = function() {
    this.collection.remove({});
    this.transform_collection.remove({});
    this.temp_index = {};
};

IndexController.prototype.index_item = function(item){
    var fields = this.fields;
    var propname, field_value, preserve, id;
    var new_obj = {};

    for (var i = 0; i < fields.length; i++) {
        // if field is array iterate over array
        propname = fields[i].propname;
        field_value = item[propname];
        preserve = fields[i].preserve;
        if (field_value instanceof Array && field_value.length > 0) {
            new_obj[propname] = [];
            for (var j = 0; j < field_value.length; j++) {
                id = this.add_record(propname, field_value[j], preserve);
                new_obj[propname].push(id);
            }
        } else {
            id = this.add_record(propname, field_value, preserve);
            new_obj[propname] = id;
        }
    }

    this.transform_to_insert.push(new_obj);

};

IndexController.prototype._build_temp_index = function(){
    this.temp_index = {};
    console.log('INDEX CONTROLLER: building temp index - start');
    var records = this.collection.find().fetch();
    var length = records.length;

    for(var i = 0; i < length; i++){
        var hash = fast_hash(records[i].r);
        var id = records[i].id;
        var field = records[i].d;

        this._ensure_field_in_temp_index(field);

        // record hash to temp index
        this.temp_index[field].hashes[hash] = id;
        var top_id = this.temp_index[field].top_id;

        // check to see if this is the top id.
        if(!top_id || radix64.decodeToInt(top_id) < radix64.decodeToInt(id) ){
            this.temp_index[field].top_id = id;
        }
    }
    console.log('INDEX CONTROLLER: building temp index - done');
};

IndexController.prototype._ensure_field_in_temp_index = function(field){
    if(!this.temp_index[field]){
        this.temp_index[field] = {
            hashes : {},
            top_id : undefined
        };
    }
};

/*
Given field,value check index collection to see if it already exists.
If not add the record and return the new id, if it exists return false.

xxx: this is horribly slow. need to refactor so that adding a record doesn't
require a full table scan (twice)

options, keep indexes storred on controller instance, still slow.
*/
IndexController.prototype.add_record = function(field, value, preserve) {
    // check to see if value exists already;
    var next_id;

    if(preserve){
        next_id = value;
    } else {
        var hash = fast_hash(value);

        /* make sure field is present in temp index */
        this._ensure_field_in_temp_index(field);

        var exists = _.has(this.temp_index[field].hashes, hash);
        //console.log('add_record: exists', exists);
        //console.log('add_record: temp_index', this.temp_index);

        if(exists){
            return this.temp_index[field].hashes[hash];
        }

        var top_id = this.temp_index[field].top_id;
        next_id = this._increment_id(top_id);

        /* update temp index */
        this.temp_index[field].hashes[hash] = next_id;
        this.temp_index[field].top_id = next_id;
    }

    this.index_to_insert.push({
        id: next_id,
        d: field,
        r: value
    });
    return next_id;
};

/*

*/
IndexController.prototype.rebuild_index = function(field, preserve) {
    var source = this.source;
    var sort = this.sort;
    var length = this.source.find({}).count();
    var collection = this.collection;

    collection.remove({
        d: field
    });


    for (var i = 0; i < length; i++) {
        var element = source.find({}, {
            skip: i,
            limit: 1
        }).fetch();

        if (element.length == 1) {

            var field_value = element[0][field];

            if (typeof field_value == "object" && field_value.length > 0) {
                for (var j = 0; j < field_value.length; j++) {
                    this.add_record(field, field_value[j], preserve);
                }
            } else {
                this.add_record(field, field_value, preserve);
            }

        }

    }
};



IndexController.prototype._increment_id = function(last_id) {
    var out;
    if (typeof last_id == "undefined") {
        out = radix64.encodeInt(0);
    } else {
        var l = radix64.decodeToInt(last_id);
        out = radix64.encodeInt(l + 1);
    }
    return out;
};

IndexController.prototype.get_compressed = function(query) {
    return this.transform_collection.find().fetch();
};

IndexController.prototype.build_assets = function() {
    var data = this.get_compressed();
    var file_meta_data = {
        timestamp: new Date().getTime(),
        length: data.length
    };
    var file_id = this.file_collection.insert(file_meta_data);
    var file_path = this.file_collection.get_path(file_id);

    fs.writeFile(file_path, JSON.stringify(data), function(err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
};
