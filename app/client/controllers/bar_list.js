BarList = function(context, options) {
    this.index_key = options.index_key;
    this.bar_height = options.bar_height || 36;
    this.margins = options.margins;
    this.class = options.element_class;
    this.div = d3.select('.' + options.element_class);
    this.width = calc_width = $('.' + options.element_class).width();
    this.result_limit = options.result_limit || 20;
    this.padding = 3;
    this.height = (this.bar_height + this.padding || 0) * this.result_limit;
    this.id = options.id;
    this.x = d3.scale.linear().range([0, this.width]);
    this.dimension = options.dimension;
    this.group = options.group;
    this._filter_list = [];

    this.set_filter = options.set_filter;
    /*if(options.result_limit){
      this.group = this.group.top(options.result_limit);
    }*/
    this.round = null;
    this.lable_rotate = options.lable_rotate || 65;

    this.get_total = options.get_total;

    this.get_data = options.get_data || function(count) {
        return this.group.all();
    }.bind(this);

    // set props that depend on other props
    var raw_y_scale = d3.scale.ordinal();
    this.get_y_scale = function() {
        return raw_y_scale.domain(_.pluck(this.group.top(this.result_limit), 'key'))
            .rangeBands([0, this.height]);
    }.bind(this);

    if (options.override_update) {
        this._update = options.override_update;
    }

    this.y = this.get_y_scale();

    this.filter = this._set_filter;

    this.init();
    this.attach_events();
};

BarList.prototype.init = function() {
    div = this.div;
    var width = this.width;
    var height = this.height;
    var margin = this.margins;
    var id = this.id;
    var group = this.group;
    var g = this.top_context = div.select('.d-list');

    this.group_total = this.get_total();

    var get_local_total = function(){
        return _.filter(this.group.all(), function(d){
            return d.value ? true : false;
        }).length || 0;
    }.bind(this);

    this.x.domain([0, group.top(1)[0].value]);

    this.y = this.get_y_scale();

    var data = this.get_data(this.result_limit);

    if (this.top_context.empty()) {

        this.title = div.append('p').attr('class','title');
        this.list_count = this.title.append('span').attr('class','list-count');

        this.list_count.datum(get_local_total).text(function(d){
            return d;
        });


        // create reste link
        div.select(".title").append("a")
            .attr("class", "reset")
            .text("reset")
            .style("display", "none");

        // add svg
        this.top_context = div.append("div").attr('class', 'd-list');

        this.bar_context = this.top_context.append('ul').attr('class', 'bars');

        this._update(data);


    } else {

        this.list_count.datum(get_local_total).text(function(d){
            return d;
        });

        this._update(data);
    }



};

BarList.prototype._update = function(data) {
    var context = this.bar_context;
    var x = this.x;
    var y = this.y;
    var width = this.width;
    var total = this.group_total;
    var index_key = this.index_key;

    var groups = context.selectAll('.bar')
        .data(data, function(d) {
            return d.key;
        });

    var on_exit = function(chain) {
        chain.exit().remove();
    };

    var on_update = function(chain) {

        chain.select('.ovr .ltext')
            .text(function(d) {
                //return Index.findOne({id: d.key, d:index_key}).r;
                return d.key;
            });

        chain.select('.ovr .lperc')
            .text(function(d) {
                return Math.round(d.value / total * 100) + "%";
            });

        chain.select('.ovr .lcount')
            .text(function(d) {
                return d.value;
            });

        chain.select('div.fg')
            .attr("style", function(d) {
                return 'width:' + (x(d.value) / width * 100) + '%';
            }.bind(this));
    }.bind(this);

    var on_enter = function(groups) {
        var bar_group = groups.enter()
            .append('li')
            .attr('class', 'bar');
        bar_group.append("div")
            .attr('class', 'bg');

        bar_group.append("div")
            .attr('class', 'fg');
        var overlay = bar_group.append('div')
            .attr('class', 'ovr');

        overlay.append('span').attr('class', 'ltext');
        overlay.append('span').attr('class', 'lcount');
        overlay.append('span').attr('class', 'lperc');


    }.bind(this);

    on_enter(groups);
    on_update(groups);
    on_exit(groups);

    groups.order();

};



BarList.prototype.attach_events = function() {
    var self = this;
    this.top_context.selectAll('.bar').on('click', function(d, i){
        console.log(this, d, i);
        var el = d3.select(this);
        el.attr('class', self.toggle_filter(d.key) ? 'filter' : null);
    });
};

BarList.prototype.toggle_filter = function(d){
    var flag = false;
    if(_.indexOf(this._filter_list, d) < 0){
        this._filter_list.push(d);
        flag = true;
    } else {
        this._filter_list = _.without(this._filter_list, d);
        flag = false;
    }
    this.filter(this._filter_list);
    return flag;
};

BarList.prototype.render = function() {
    // if no brush active on this graph resort
    this._set_x(this.x_scale);

    this.init();

};

BarList.prototype._set_x = function(x) {
    var x_scale = this.x();
    //this.axis.scale(x_scale);
    //this.brush.x(x_scale);
    return x_scale;
};

BarList.prototype._set_filter = function(_) {
    if (_) {
        //this.brush.extent(_);
        this.dimension.filterRange(_);
    } else {
        //this.brush.clear();
        this.dimension.filterAll();
    }
    //this.brushDirty = true;
};

parseUri = function(str) {
    var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};
