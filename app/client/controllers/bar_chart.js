

BarChart = function(context, options) {
    this.width = calc_width = $('.' + options.element_class).width();
    this.padding = 4;


    this.height = options.height;
    this.margins = options.margins;

    console.log(this.width, this.height);
    this.div = d3.select('.' + options.element_class);

    this.id = options.id;

    this.axis = d3.svg.axis().orient("bottom");
    this.y = d3.scale.linear().range([this.height, 0]);

    this.brush = d3.svg.brush();
    this.brushDirty = true;

    this.dimension = options.dimension;
    this.group = options.group;
    this.set_filter = options.set_filter;

    this.result_limit = this.group.all().length;
    this.bar_width = this.width / this.result_limit - this.padding;

    this.round = null;
    this.lable_rotate = options.lable_rotate || 65;

    this.get_data = options.get_data || function(count) {
        return this.group.all();
    }.bind(this);

    var raw_x_scale = d3.scale.linear();
    this.get_x_scale = function() {
        return raw_x_scale.domain([0, 24])
            .range([0, this.width]);
    }.bind(this);

    // set props that depend on other props
    this.x = this.get_x_scale();

    this.filter = this._set_filter;

    this.init();
    this.attach_events();
};

BarChart.prototype.init = function() {
    div = this.div;
    var width = this.width;
    var height = this.height;
    var margin = this.margins;
    var id = this.id;
    var brush = this.brush;
    var brushDirty;
    var group = this.group;
    var g = this.top_context = div.select('g');

    this.y.domain([0, group.top(1)[0].value]);
    var x;

    this.x = this.get_x_scale();
    this.axis.scale(this.x);
    this.brush.x(this.x);

    var data = this.get_data(this.result_limit);

    if (this.top_context.empty()) {

        // create reste link
        div.append('div').attr('class','title');
        
        div.select(".title").append("a")
            .attr("class", "reset")
            .text("reset")
            .style("display", "none");

        // add svg
        this.top_context = div.append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        this.bar_context = this.top_context.append('g').attr('class', 'bars');
        this.lable_context = this.top_context.append('g').attr('class', 'lables');
        var context = this.top_context;

        // add axis lables
        this.axis_context = this.top_context.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + this.height + ")");



        this._update_bars(data);
        this._update_lables(data);
        this._update_axis(data);


        // Initialize the brush component with pretty resize handles.
        this.brush_context = this.top_context.append("g").attr("class", "brush").call(brush);
        this.brush_context.selectAll("rect").attr("height", height);
        this.brush_context.selectAll(".resize").append("path").attr("d", this._brush_handles.bind(this));
    } else {

        this._update_bars(data);
        this._update_lables(data);
        this._update_axis(data);
    }

    // Only redraw the brush if set externally.
    if (this.brushDirty) {
        this.brushDirty = false;
        this.top_context.selectAll(".brush").call(brush);

        div.select(".title a").style("display", brush.empty() ? "none" : null);
        if (brush.empty()) {
            g.selectAll("#clip-" + id + " rect")
                .attr("x", 0)
                .attr("width", width);
        } else {
            var extent = brush.extent();
            g.selectAll("#clip-" + id + " rect")
                .attr("x", this.x(extent[0]))
                .attr("width", this.x(extent[1]) - this.x(extent[0]));
        }
    }

};

BarChart.prototype._update_bars = function(data) {
    var context = this.bar_context;
    var x = this.x;

    var rects = context.selectAll('rect')
        .data(data);

    // update
    rects.attr("transform", function(d, i) {
            return "translate(" + x(d.key) + "," + this.y(d.value) + ")";
        }.bind(this))
        .attr("width", this.bar_width)
        .attr("height", function(d) {
            return this.height - this.y(d.value);
        }.bind(this));

    //enter
    rects.enter().append("rect")
        .attr("transform", function(d, i) {
            return "translate(" + x(d.key) + "," + this.y(d.value) + ")";
        }.bind(this))
        .attr("width", this.bar_width)
        .attr("height", function(d) {
            return this.height - this.y(d.value);
        }.bind(this));

    // exit
    rects.exit().remove();

};

BarChart.prototype._update_lables = function(data) {
    var context = this.lable_context;
    var x = this.x;

    var texts = context.selectAll('text')
        .data(data, function(d) {
            return d.key;
        });

    // update
    texts.attr("x", function(d, i) {
            return x(d.key);
        }.bind(this))
        .text(function(d) {
            return Math.round(d.value);
        })
        .attr("y", function(d) {
            return this.y(d.value) + 14;
        }.bind(this));

    //enter
    texts.enter()
        .append('text')
        .text(function(d) {
            return Math.round(d.value);
        })
        .attr("text-anchor", "middle")
        .attr("x", function(d, i) {
            return x(d.key);
        }.bind(this))
        .attr("y", function(d) {
            return this.y(d.value) + 14;
        }.bind(this))
        .attr('dx', this.bar_width / 2)
        .attr("font-family", "sans-serif")
        .attr("font-size", "11px")
        .attr("fill", "white");

    // exit
    texts.exit().remove();

};

BarChart.prototype._update_axis = function(data) {
    var context = this.axis_context;

    context.call(this.axis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", function(d) {
            return "rotate(-65)";
        });
};

BarChart.prototype._brush_handles = function(d) {
    var e = +(d == "e"),
        x = e ? 1 : -1,
        y = this.height / 3;
    return "M" + (.5 * x) + "," + y + "A6,6 0 0 " + e + " " + (6.5 * x) + "," + (y + 6) + "V" + (2 * y - 6) + "A6,6 0 0 " + e + " " + (.5 * x) + "," + (2 * y) + "Z" + "M" + (2.5 * x) + "," + (y + 8) + "V" + (2 * y - 8) + "M" + (4.5 * x) + "," + (y + 8) + "V" + (2 * y - 8);
};



BarChart.prototype.attach_events = function() {
    var brush = this.brush;
    var round = this.round;
    var x = this.x;
    var id = this.id;
    var dimension = this.dimension;
    var div = this.div;

    brush.on("brushstart.chart", function() {
        var div = d3.select(this.parentNode.parentNode.parentNode);
        div.select(".title a").style("display", null);
    });

    brush.on("brush.chart", function() {
        var g = this.div.select('g');
        var extent = brush.extent();

        console.log('BRUSH');
        this.set_filter(extent);


    }.bind(this));

    brush.on("brushend.chart", function() {
        if (brush.empty()) {
            var div = d3.select(this.parentNode.parentNode.parentNode);
            div.select(".title a").style("display", "none");
            div.select("#clip-" + id + " rect").attr("x", null).attr("width", "100%");
            //dimension.filterAll();
        }
    });
};

BarChart.prototype.render = function() {
    // if no brush active on this graph resort
    this._set_x(this.x_scale);

    this.init();

};

BarChart.prototype._set_x = function(x) {
    var x_scale = this.x();
    this.axis.scale(x_scale);
    this.brush.x(x_scale);
    return x_scale;
};

BarChart.prototype._set_filter = function(_) {
    if (_) {
        this.brush.extent(_);
        this.dimension.filterRange(_);
    } else {
        this.brush.clear();
        this.dimension.filterAll();
    }
    this.brushDirty = true;
};
