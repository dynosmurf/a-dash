// Various formatters.
var formatNumber = d3.format(",d"),
    formatChange = d3.format("+,d"),
    formatDate = d3.time.format("%B %d, %Y"),
    formatTime = d3.time.format("%I:%M %p");

// A nest operator, for grouping the request list.
var nestByRequest = d3.nest().key(function(v) {
    return v.r_id;
});

/* Page controller */

PageController = function(vectors) {
    console.log('Parsed');
    console.log(vectors.length);

    if(vectors.length === 0){
        return;
    }
    var context = this.context = crossfilter(vectors);

    /* dimensions */
    var hour = context.dimension(function(v) {
        var h = new Date(v.timestamp);
        return h.getHours() + h.getMinutes() / 60;
    });

    var url = context.dimension(function(v) {
        return v.url;
    });

    var param = context.dimension(function(v) {
        return v.params;
    });

    var pair = context.dimension(function(v) {
        return v.pairs;
    });

    var request = context.dimension(function(v) {
        return v._id;
    });

    this.dimensions = {
        hour: hour,
        url: url,
        param: param,
        pair: pair,
        request: request
    };


    // groups
    var all = context.groupAll();

    var urls = url.group();
    // add custom reduce to this one..
    var hours = hour.group(Math.floor);


    var get_reduce = function(key) {
        return {
            add: function(p, v) {
                v[key].forEach(function(val, idx) {
                    p[val] = (p[val] || 0) + 1; //increment counts
                });
                return p;

            },
            remove: function(p, v) {
                v[key].forEach(function(val, idx) {
                    p[val] = (p[val] || 0) - 1; //decrement counts
                });
                return p;

            },
            initial: function() {
                return {};
            }
        };
    };
    var params_reduce = get_reduce('params');
    var params = param.groupAll()
        .reduce(params_reduce.add, params_reduce.remove, params_reduce.initial)
        .value();

    params.all = function() {
        var newObject = [];
        for (var key in this) {
            if (this.hasOwnProperty(key) && key != "all" && key != "top") {
                newObject.push({
                    key: key,
                    value: this[key]
                });
            }
        }
        return newObject;
    };

    params.top = function(count) {
        var all = this.all();
        all = _.sortBy(all, function(v) {
            return v.value;
        });
        return all.reverse().slice(0, count);
    };


    var pairs_reduce = get_reduce('pairs');
    var pairs = pair.groupAll()
        .reduce(pairs_reduce.add, pairs_reduce.remove, pairs_reduce.initial)
        .value();

    pairs.all = function() {
        var newObject = [];
        for (var key in this) {
            if (this.hasOwnProperty(key) && key != "all" && key != "top") {
                newObject.push({
                    key: key,
                    value: this[key]
                });
            }
        }
        return newObject;
    };

    pairs.top = function(count) {
        var all = this.all();
        all = _.sortBy(all, function(v) {
            return v.value;
        });
        return all.reverse().slice(0, count);
    };

    var requests = request.group(function(r) {
        return r;
    });


    this.groups = {
        all: all,
        urls: urls,
        hours: hours,
        params: params,
        requests: requests,
        pairs: pairs
    };
    this.charts = {};
    this.build_ui();
};



PageController.prototype.build_ui = function() {

    // BY TIME
    this.charts.requests_by_time_chart = new BarChart(this.context, {
        index_key:'timestamp',
        width: 800,
        height: 200,
        bar_width: 28,
        margins: this._get_margins(0, 0, 40, 0),
        element_class: 'activity-chart',
        id: 1,
        dimension: this.dimensions.hour,
        group: this.groups.hours,
        get_data: function(count) {
            return this.groups.hours.all();
        }.bind(this),
        x_scale: function() {
            return d3.scale.linear().domain([0, 24]).rangeRound([0, 800]);
        },
        set_filter: function(extent) {
            var min = Math.round(extent[0]);
            var max = Math.round(extent[1]);
            this.dimension.filterRange([min, max]);
            page_controller.renderAll();
        },
        get_total : function(){
            return _.filter(this.groups.urls.all(), function(d){
                return d.value ? true : false;
            }).length || 0;
        }.bind(this),
    });

    $('.activity-chart .reset').on('click', this.reset.bind(this, 'requests_by_time_chart'));


    this.charts.requests_by_url_chart = new BarList(this.context, {
        index_key:'url',
        width: 800,
        height: 200,
        bar_width: 20,
        margins: this._get_margins(0, 0, 0, 0),
        element_class: 'url-list',
        lable_rotate: 90,
        id: 1,
        result_limit: 40,
        dimension: this.dimensions.url,
        group: this.groups.urls,
        get_total : function(){
            return _.filter(this.groups.urls.all(), function(d){
                return d.value ? true : false;
            }).length || 0;
        }.bind(this),
        get_data: function(count) {
            var data = _.filter(this.group.top(this.result_limit), function(d) {
                if (d.value !== 0) {
                    return true;
                }
                return false;
            });
            return data;
        },
        set_filter: function(extent) {
            var min = Math.round(extent[0]);
            var max = Math.round(extent[1]);
            this.dimension.filterRange([min, max]);
            var range_map = this.x.range();
            var domain_map = this.x.domain();
            // range_map in same units as extent. find inclued indicies
            var active_range = [];
            var active_domain = [];
            _.each(range_map, function(r, i) {
                if (extent[0] < r && extent[1] > r) {
                    active_range.push(i);
                    active_domain.push(domain_map[i]);
                }
            });

            this.dimension.filter(function(d) {
                var out = false;
                if (_.indexOf(active_domain, d) >= 0) {
                    out = true;
                }
                return out;
            });
        },
        override_update: function(data) {
            var context = this.bar_context;
            var x = this.x;
            var y = this.y;
            var width = this.width;
            var total = this.group_total;
            var index_key = this.index_key;

            var groups = context.selectAll('.bar')
                .data(data, function(d) {
                    return d.key;
                });

            var on_exit = function(chain) {
                chain.exit().remove();
            };

            var on_update = function(chain) {
                chain.select('.ovr .lhead')
                    .text(function(d) {
                        //return Index.findOne({id: d.key, d:index_key}).r;
                        return parseUri(d.key).path;
                    });
                chain.select('.ovr .ltext')
                    .text(function(d) {
                        return parseUri(d.key).query;
                    });

                chain.select('.ovr .lperc')
                    .text(function(d) {
                        return Math.round(d.value / total * 100) + "%";
                    });

                chain.select('.ovr .lcount')
                    .text(function(d) {
                        return d.value;
                    });

                chain.select('div.fg')
                    .attr("style", function(d) {
                        return 'width:' + (x(d.value) / width * 100) + '%';
                    }.bind(this));

            }.bind(this);

            var on_enter = function(groups) {
                var bar_group = groups.enter()
                    .append('li')
                    .attr('class', 'bar');
                bar_group.append("div")
                    .attr('class', 'bg');

                bar_group.append("div")
                    .attr('class', 'fg');
                var overlay = bar_group.append('div')
                    .attr('class', 'ovr');

                overlay.append('span').attr('class', 'lhead');
                overlay.append('span').attr('class', 'ltext');
                overlay.append('span').attr('class', 'lcount');
                overlay.append('span').attr('class', 'lperc');


            }.bind(this);

            on_enter(groups);
            on_update(groups);
            on_exit(groups);

            groups.order();
        }

    });




    this.charts.requests_by_param_chart = new BarList(this.context, {
        index_key:'params',
        width: 800,
        height: 200,
        bar_width: 20,
        margins: this._get_margins(0, 0, 0, 0),
        result_limit : 40,
        element_class: 'param-list',
        id: 2,
        dimension: this.dimensions.param,
        group: this.groups.params,
        y_scale: function(group, scale, range) {
            return scale.domain(_.pluck(group.top(this.result_count), 'key')).rangeBands(this.range);
        }.bind(null, this.groups.params, d3.scale.ordinal(), [0, 400]),
        get_data: function(count) {
            return this.groups.params.top(20);
        }.bind(this),
        set_filter: function(extent) {
            var min = Math.round(extent[0]);
            var max = Math.round(extent[1]);
            this.dimension.filterRange([min, max]);
            var range_map = this.x.range();
            var domain_map = this.x.domain();
            // range_map in same units as extent. find inclued indicies
            var active_range = [];
            var active_domain = [];
            _.each(range_map, function(r, i) {
                if (extent[0] < r && extent[1] > r) {
                    active_range.push(i);
                    active_domain.push(domain_map[i]);
                }
            });

            this.dimension.filter(function(d) {
                var out = false;
                _.each(active_domain, function(v) {
                    if (_.indexOf(d, v) >= 0) {
                        out = true;
                    }
                });

                return out;
            });
        },
        get_total : function(){
            return _.filter(this.groups.urls.all(), function(d){
                return d.value ? true : false;
            }).length || 0;
        }.bind(this),
    });




    this.charts.requests_by_pair_chart = new BarList(this.context, {
        index_key:'pairs',
        bar_height: 20,
        margins: this._get_margins(0, 0, 0, 0),
        element_class: 'pair-list',
        id: 3,
        dimension: this.dimensions.pair,
        group: this.groups.pairs,
        y_scale: function(group, scale, range) {
            return scale.domain(_.pluck(group.top(this.result_count), 'key')).rangeBands(this.range);
        }.bind(null, this.groups.params, d3.scale.ordinal(), [0, 400]),
        get_data: function(count) {
            return this.groups.pairs.top(20);
        }.bind(this),
        set_filter: function(extent) {
            var min = Math.round(extent[0]);
            var max = Math.round(extent[1]);
            this.dimension.filterRange([min, max]);
            var range_map = this.x.range();
            var domain_map = this.x.domain();
            // range_map in same units as extent. find inclued indicies
            var active_range = [];
            var active_domain = [];
            _.each(range_map, function(r, i) {
                if (extent[0] < r && extent[1] > r) {
                    active_range.push(i);
                    active_domain.push(domain_map[i]);
                }
            });

            this.dimension.filter(function(d) {
                var out = false;
                _.each(active_domain, function(v) {
                    if (_.indexOf(d, v) >= 0) {
                        out = true;
                    }
                });

                return out;
            });
        },
        get_total : function(){
            return _.filter(this.groups.urls.all(), function(d){
                return d.value ? true : false;
            }).length || 0;
        }.bind(this),
    });

    // global reset
    /*
    _.each(this.charts, function(chart_controller) {
        chart_controller.brush.on("brush", this.renderAll.bind(this)).on("brushend", this.renderAll.bind(this));
    }.bind(this));
    */
};

PageController.prototype._get_margins = function(top, left, bottom, right) {
    return {
        top: top,
        left: left,
        bottom: bottom,
        right: right
    };
};

PageController.prototype.renderAll = function() {
    _.each(this.charts, function(chart) {
        chart.render();
    });
};

PageController.prototype.reset = function(name) {
    this.charts[name].filter(null);
    this.renderAll();
};
