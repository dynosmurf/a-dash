/**
 * TEMPLATE: main
 */


Template.main.events({

});

Template.main.helpers({
    'sites': function() {
        var hosts = Index.find({d:'host'}).fetch();
        var sites = _.map(hosts, function(host) {
            var o = {};
            o.name = host.r;
            o.count = 0;
            return o;
        });
        sites = _.sortBy(sites, function(s){
            return s.count;
        }).reverse();
        return sites;
    },
    'site_total': function() {
        var count = _.reduce(this, function(p, d) {
            return p + d.count;
        });
        return count;
    },
    'host': function() {
        var host = Session.get('host');
        return Hosts.findOne(host);
    },
    'site': function() {
        var site = Session.get('site');
        return site;
    },
});

Template.main.onRendered(function() {
    var host = Session.get('host');
    var site = Session.get('site');

    var vectors;
    vectors = Index.find().fetch();
    Meteor.call('get_compressed', function(err, result){
        console.log(result);
        page_controller = this.controller = new PageController(result);
    }.bind(this));

});

Template.main.onCreated(function() {


});

Template.main.onDestroyed = (function() {

});
