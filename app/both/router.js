var log_render_time = function(t){
    Logs.insert({
        type:'render_time',
        time : new Date().getTime(),
        data : {
            render_time: t,
            request_count : Requests.find().count(),
            host : Session.get('host'),
            site : Session.get('site')
        }
    });
};

Router.route('/', function() {
    // add the subscription handle to our waitlist
    //this.wait(Meteor.subscribe('requests_all'));
    this.wait(Meteor.subscribe('host-index'));
    //this.wait(Meteor.subscribe('trans'));

    // this.ready() is true if all items in the wait list are ready
    if (this.ready()) {
        console.log('RENDER');
        console.log(new Date().getTime() - timer);
        Session.set('host', null);
        this.render('main');

    } else {
        this.timer = new Date().getTime();

        timer =
        console.log('LOADING');
        this.render('loading');
    }

});

Router.route('/site/:site', function() {
    // add the subscription handle to our waitlist
    this.wait(Meteor.subscribe('requests_by_site', this.params.site));
    this.wait(Meteor.subscribe('hosts_all'));
    this.wait(Meteor.subscribe('index'));
    this.wait(Meteor.subscribe('trans'));

    // this.ready() is true if all items in the wait list are ready
    if (this.ready()) {
        console.log('RENDER');
        console.log('RNEDER TIME: ', new Date().getTime() - this.timer, this.timer);

        Session.set('site', this.params.site);
        this.render('main');

    } else {

        this.timer = new Date().getTime();
        console.log('LOADING');
        this.render('loading');
    }
});


Router.route('/site/domain/:host', function() {
    // add the subscription handle to our waitlist
    this.wait(Meteor.subscribe('requests_by_host', this.params.host));
    this.wait(Meteor.subscribe('hosts_all'));

    // this.ready() is true if all items in the wait list are ready
    if (this.ready()) {
        console.log('RENDER');
        Log.insert({t: new Date().getTime()})
        console.log('RNEDER TIME: ', new Date().getTime() - this.timer, this.timer);

        Session.set('host', this.params.host);
        this.render('main');

    } else {

        this.timer = new Date().getTime();
        console.log('LOADING');
        this.render('loading');
    }
});
