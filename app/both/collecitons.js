Requests = new Mongo.Collection('requests');
Hosts =  new Mongo.Collection('hosts');

Logs = new Mongo.Collection('logs');

Index = new Mongo.Collection('index');
Transform = new Mongo.Collection('transform');

if(Meteor.isServer){
    DataAssets = new FileCollection('data-assets',{
        path : 'data', // stored at, relative to app dir
        directory : '/d/' // served at
    });
}
