Meteor.startup(function () {
    Requests._ensureIndex({ "host": 1});
    Index._ensureIndex({ "d": 1});
    Index._ensureIndex({ "id": 1});
});
