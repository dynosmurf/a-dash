Meteor.publish('host-index', function(){
    return Index.find({d:'host'},{
    });
});

Meteor.publish('trans', function(){
    return Transform.find({},{
        limit : 1000
    });
});

Meteor.publish('requests_by_host', function(host) {
    console.log('#PUB requests_by_host: ', host);
    return Requests.find({
        host: host
    }, {
        fields: {
            'url': 1,
            'timestamp': 1,
            'host': 1
        },
        skip: 0,
        limit: 100000
    });
});

Meteor.publish('requests_by_site', function(site) {
    console.log('#PUB requests_by_site: ', site);
    return Requests.find({
        site:site
    }, {
        fields: {
            'url': 1,
            'timestamp': 1,
            'site' : 1
        },
        skip: 0,
        limit: 100000
    });
});

Meteor.publish('requests_all', function() {
    console.log('#PUB requests_all: ');
    return Requests.find({}, {
        fields: {
            'url': 1,
            'timestamp': 1,
            'host': 1,
            'site': 1
        },
        skip: 0,
        limit: 100000
    });
});

Meteor.publish('hosts_all', function() {
    console.log('#PUB hosts_all: ');
    return Hosts.find({}, {
        fields: {
            'name': 1,
            'count': 1,
            'site': 1,
        },
    });
});
