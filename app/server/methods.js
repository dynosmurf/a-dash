var URL = Npm.require('url');

var index_controller = new IndexController({
    source_collection: Requests,
    sort: {
        timestamp: 1
    },
    file_collection: DataAssets,
    index_collection: Index,
    transform_collection: Transform,
    fields: [{
        propname: 'url',
        preserve: false,
    }, {
        propname: 'pairs',
        preserve: false,
    }, {
        propname: 'params',
        preserve: false,
    }, {
        propname: 'timestamp',
        preserve: true
    }, {
        propname: 'host',
        preserve: false
    }, {
        propname: 'site',
        preserve: false
    }]
});

var get_requests_as_json = function(uri, callback){
    HTTP.call("GET", uri, function(err, result) {

        try {
            data = JSON.parse(result.content);
        } catch (e) {
            console.log('error', err, result.content.length);
            console.log(e);
            callback(e);
        }

        callback(null, data);
    });
};

var format_raw_request = function(raw){
    var host = URL.parse(raw.SCRIPT_URI).hostname;
    var h = host.split('.');

    // tw
    var site = h[h.length - 2];

    var url = raw.SCRIPT_URI + '?' + raw.QUERY_STRING;
    var parsed = URL.parse(url);
    var pairs = [];
    var params = [];

    pairs = parsed.query.split('&');
    params = _.map(pairs, function(pair) {
        return pair.split('=')[0].replace('amp;', '');
    });

    var request = {
        _id: raw._id,
        url: url,
        host: host,
        params: params,
        pairs: pairs,
        site: site,
        /* xxx this is part of raw now */
        timestamp: new Date().getTime() - _.random(0, 1000 * 60 * 60 * 24 * 10)
    };
    return request;
};

/*
{
       "_id": "56391a8e120fe40100436cd1",
       "HTTP_USER_AGENT": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
       "QUERY_STRING": "neighborhood=837517&feature=Recommended&sortType=commentCount&readerRating=0&locationCategory=1020291",
       "REMOTE_ADDR_ORIG": "10.24.2.90",
       "QS_elements": [
           "neighborhood=837517",
           "feature=Recommended",
           "sortType=commentCount",
           "readerRating=0",
           "locationCategory=1020291"
       ],
       "UNIQUE_ID": "VjkaiQoYAnUAAWaUCd8AAAdF",
       "REMOTE_ADDR": "66.249.69.66",
       "HTTP_X_FORWARDED_FOR": "66.249.69.66",
       "SCRIPT_URL": "/chicago/LocationSearch",
       "REQUEST_URI": "/chicago/LocationSearch?neighborhood=837517&feature=Recommended&sortType=commentCount&readerRating=0&locationCategory=1020291",
       "HTTP_FROM": "googlebot(at)googlebot.com",
       "SCRIPT_URI": "http://www.chicagoreader.com/chicago/LocationSearch"
},
*/

Meteor.methods({
    'get_compressed': function() {
        console.log('#METHOD > GET COMPRESSED');
        return index_controller.get_compressed();
    },
    'build_asset': function() {
        index_controller.build_assets();
    },
    'get_data': function() {
        console.log('#METHOD > GET DATA');

        get_requests_as_json("http://127.0.0.1:8011/test.json", function(err, result){
            /* xxx limit number for testing */
            var raw_requests = result.slice(0,100000);
            var formated_request;
            var finished = [];

            /* clear out old data */

            Hosts.remove({});
            Requests.remove({});
            index_controller.clear_all();

            var requests_to_insert = [];

            for(var i =0; i < raw_requests.length; i++){
                formated_request = format_raw_request(raw_requests[i]);

                requests_to_insert.push(formated_request);

                var id = index_controller.index_item(formated_request);
                finished.push(id);

                /* console logging */
                if(finished.length % 20 === 0 ){
                    console.log(finished.length);
                }

                /* console logging */
                if(finished.length % 1000 === 0 ){
                    console.log(finished.length);
                    Requests.batchInsert(requests_to_insert);
                    requests_to_insert = [];
                    index_controller.persist_to_db();
                }

                if(finished.length == raw_requests.length){
                    console.log('done...');

                    index_controller.persist_to_db();

                    console.log(index_controller.dimension('site'));
                    console.log(index_controller.dimension('host'));
                }
            }
        });

    },
    'clear_data': function(code) {
        if(code == "yep"){
            Requests.remove({});
            Index.remove({});
            Transform.remove({});
        }
    },
});
